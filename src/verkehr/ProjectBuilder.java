package verkehr;

import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

public class ProjectBuilder implements ContextBuilder<Object> {

	public static final int CAR = 1;
	public static final int TRAIN = 2;

	private Parameter para = null;

	@Override
	public Context<Object> build(Context<Object> context) {
		// Hole Parameter
		getInitParams();

		// Erstelle Overseer
		Overseer o = new Overseer(para);
		context.add(o);

		// Erstelle Pendler
		for (int i = 0; i < para.getPendlerCap(); i++) {
			context.add(new Pendler(o, para));
		}

		int endAt = (Integer) RunEnvironment.getInstance().getParameters()
				.getValue("endAt");
		RunEnvironment.getInstance().endAt(endAt);

		return context;
	}

	/**
	 * Initiale Parameter werden geladen
	 */
	private void getInitParams() {
		Parameters p = RunEnvironment.getInstance().getParameters();
		para = new Parameter();

		para.setPendlerCap(p.getInteger("pendler_cap"));

		para.setCarCap(p.getInteger("car_cap"));
		para.setTrainCap(p.getInteger("train_cap"));

		para.setDeterministic(p.getBoolean("choice"));

		para.setPopChange(p.getBoolean("popChange"));
		para.setPopChangeValue(p.getInteger("popChangeValue"));

		para.setCar_bonus(p.getDouble("car_bonus"));
		para.setTrain_bonus(p.getDouble("train_bonus"));

		para.setOptimize_city(p.getBoolean("optimize_city"));

		para.setHappines_threshold(p.getDouble("happines_threshold"));

		para.setDeveriation(p.getDouble("normal"));
	}
}
