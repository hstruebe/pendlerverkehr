package verkehr;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.util.ContextUtils;

public class Overseer {
	private Parameter p = null;

	private int CarUser = 0;
	private int TrainUser = 0;

	private double StreetUse = 0;
	private double RailsUse = 0;

	private double lowPercent = 0.7;

	public Overseer(Parameter para) {
		p = para;
		System.out.println("init");
		calc();
	}

	@ScheduledMethod(start = 1.1, interval = 1.0)
	public void step() {
		System.out.println("Schedule Overseer");

		if (p.isPopChange()) {
			int change = p.getPopChangeValue();
			if (change == 0)
				change = RandomHelper.nextIntFromTo(-2, 2);

			if (change > 0)
				createNewPendler(change);
			else if (change < 0)
				removePendler(change);
		}

		if (p.isOptimize_city())
			optimize_city();

		calc();
		System.out.println("Nutzer: " + CarUser + " " + TrainUser);

		CarUser = 0;
		TrainUser = 0;
	}

	private void optimize_city() {
		if (CarUser > p.getCarCap()
				&& ((int) RunEnvironment.getInstance().getCurrentSchedule()
						.getTickCount()) % 10 == 0) {

			p.setCarCap((int) (p.getCarCap() + (p.getCarCap()) * 0.1));
		}

		if (TrainUser > p.getTrainCap()) {
			// Erhoehe Kapazitaet
			p.setTrainCap(p.getTrainCap() + 2);
		} else if (TrainUser <= (p.getTrainCap() * lowPercent)) {
			// Verringere Kapazitaet
			p.setTrainCap((int) (p.getTrainCap() * 0.95));
		}
	}

	/**
	 * Kalkuliert den Nutzen des Verkehrsmittels
	 */
	private void calc() {
		StreetUse = Math
				.max(1.0d - Math.pow(
						((double) CarUser / (double) p.getCarCap()), 2), 0);
		RailsUse = Math.max(1.0d - Math.pow(
				((double) TrainUser / (double) p.getTrainCap()), 2), 0);

		if (Double.isNaN(StreetUse)) {
			StreetUse = 0.0d;
		}
		if (Double.isNaN(RailsUse)) {
			RailsUse = 0.0d;
		}
		
		StreetUse += p.getCar_bonus();
		RailsUse += p.getCar_bonus();
		
		System.out.println("Neu berechnet: " + StreetUse + " und " + RailsUse);
	}

	/**
	 * @param Type
	 */
	public void inc(int Type) {
		if (Type == ProjectBuilder.CAR) {
			CarUser++;
		} else if (Type == ProjectBuilder.TRAIN) {
			TrainUser++;
		} else {
			System.err.println("Wrong Transportation Type");
		}
	}

	/**
	 * Gibt Nutzen zurueck
	 * 
	 * @param Type
	 *            Typ des Verkehrsmittels
	 * @return Berechneter Nuzten
	 */
	public double getUse(int Type) {
		if (Type == ProjectBuilder.CAR) {
			return StreetUse;
		} else if (Type == ProjectBuilder.TRAIN) {
			return RailsUse;
		} else {
			return -1.0;
		}
	}

	private boolean createNewPendler(int count) {
		if (count == 0 || count > 10)
			return false;

		@SuppressWarnings("unchecked")
		Context<Object> context = ContextUtils.getContext(this);

		for (int i = 0; i < count; i++)
			context.add(new Pendler(this, p));

		return true;
	}

	private boolean removePendler(int count) {
		if (count == 0 || count < -10)
			return false;

		@SuppressWarnings("unchecked")
		Context<Object> context = ContextUtils.getContext(this);

		for (int i = 0; i > count; i--) {
			Object o = context.getRandomObject();
			if (o instanceof Pendler) {
				context.remove(o);
			} else
				i++;
		}

		return true;
	}

	public int getTotalUsers() {
		return CarUser + TrainUser;
	}

	public int getCarUsers() {
		return CarUser;
	}

	public int getTrainUsers() {
		return TrainUser;
	}

	public int getCarCap() {
		return p.getCarCap();
	}

	public int getTrainCap() {
		return p.getTrainCap();
	}

}
