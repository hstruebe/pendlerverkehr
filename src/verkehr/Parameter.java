package verkehr;

public class Parameter {

	private int PendlerCap = 0;

	private int CarCap = 0;
	private int TrainCap = 0;

	private boolean deterministic = true;

	private boolean popChange = false;
	private int popChangeValue = 0;

	private double car_bonus = 0;
	private double train_bonus = 0;

	private boolean optimize_city = false;

	private double happines_threshold = 0;

	private double deveriation = 0.3d;

	public Parameter() {

	}

	/**
	 * @return the pendlerCap
	 */
	public int getPendlerCap() {
		return PendlerCap;
	}

	/**
	 * @param pendlerCap
	 *            the pendlerCap to set
	 */
	public void setPendlerCap(int pendlerCap) {
		PendlerCap = pendlerCap;
	}

	/**
	 * @return the carCap
	 */
	public int getCarCap() {
		return CarCap;
	}

	/**
	 * @param carCap
	 *            the carCap to set
	 */
	public void setCarCap(int carCap) {
		CarCap = carCap;
	}

	/**
	 * @return the trainCap
	 */
	public int getTrainCap() {
		return TrainCap;
	}

	/**
	 * @param trainCap
	 *            the trainCap to set
	 */
	public void setTrainCap(int trainCap) {
		TrainCap = trainCap;
	}

	/**
	 * @return the deterministic
	 */
	public boolean isDeterministic() {
		return deterministic;
	}

	/**
	 * @param deterministic
	 *            the deterministic to set
	 */
	public void setDeterministic(boolean deterministic) {
		this.deterministic = deterministic;
	}

	/**
	 * @return the popChange
	 */
	public boolean isPopChange() {
		return popChange;
	}

	/**
	 * @param popChange
	 *            the popChange to set
	 */
	public void setPopChange(boolean popChange) {
		this.popChange = popChange;
	}

	/**
	 * @return the popChangeValue
	 */
	public int getPopChangeValue() {
		return popChangeValue;
	}

	/**
	 * @param popChangeValue
	 *            the popChangeValue to set
	 */
	public void setPopChangeValue(int popChangeValue) {
		this.popChangeValue = popChangeValue;
	}

	/**
	 * @return the car_bonus
	 */
	public double getCar_bonus() {
		return car_bonus;
	}

	/**
	 * @param car_bonus
	 *            the car_bonus to set
	 */
	public void setCar_bonus(double car_bonus) {
		this.car_bonus = car_bonus;
	}

	/**
	 * @return the train_bonus
	 */
	public double getTrain_bonus() {
		return train_bonus;
	}

	/**
	 * @param train_bonus
	 *            the train_bonus to set
	 */
	public void setTrain_bonus(double train_bonus) {
		this.train_bonus = train_bonus;
	}

	/**
	 * @return the optimize_city
	 */
	public boolean isOptimize_city() {
		return optimize_city;
	}

	/**
	 * @param optimize_city
	 *            the optimize_city to set
	 */
	public void setOptimize_city(boolean optimize_city) {
		this.optimize_city = optimize_city;
	}

	/**
	 * @return the happines_threshold
	 */
	public double getHappines_threshold() {
		return happines_threshold;
	}

	/**
	 * @param happines_threshold
	 *            the happines_threshold to set
	 */
	public void setHappines_threshold(double happines_threshold) {
		this.happines_threshold = happines_threshold;
	}

	/**
	 * @return the deveriation
	 */
	public double getDeveriation() {
		return deveriation;
	}

	/**
	 * @param deveriation
	 *            the deveriation to set
	 */
	public void setDeveriation(double deveriation) {
		this.deveriation = deveriation;
	}
}
