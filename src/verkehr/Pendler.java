package verkehr;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;

public class Pendler {

	private Parameter p = null;

	private double preference;

	private int used;

	private Overseer overseer;

	public Pendler(Overseer o, Parameter para) {
		preference = RandomHelper.nextDoubleFromTo(-1.0, 1.0);
		overseer = o;
		p = para;
	}

	@ScheduledMethod(start = 1.0, interval = 1.0)
	public void step() {
		// TODO: Berechne neue Praeferenz
		newPreference(used);
		// Waehle Nutzen anhand Praeferenz
		if (p.isDeterministic())
			used = deterministicChoice();
		else {
			used = randomChoice();
		}

		overseer.inc(used);
	}

	private void newPreference(int Type) {
		double satisfaction = (p.getHappines_threshold() - overseer
				.getUse(used)) / 100;

		switch (used) {
		case ProjectBuilder.TRAIN:
			this.preference -= satisfaction;
			break;
		case ProjectBuilder.CAR:
			this.preference += satisfaction;
			break;
		default:
			break;
		}

		// Preference too high
		if (this.preference < -1.0d) {
			this.preference = -1.0d;
		} else if (this.preference > 1.0d) {
			this.preference = 1.0d;
		}

	}

	/**
	 * @return Gewaehlte Verkehrsmittelwahl
	 */
	private int deterministicChoice() {
		if (preference <= 0.0d) {
			return ProjectBuilder.CAR;
		} else {
			return ProjectBuilder.TRAIN;
		}
	}

	/**
	 * @return
	 */
	private int randomChoice() {
		// Normalverteilung um die Preference mit Varianz
		double n = RandomHelper.createNormal(preference, p.getDeveriation())
				.nextDouble();

		if (n <= 0.0d) {
			return ProjectBuilder.CAR;
		} else {
			return ProjectBuilder.TRAIN;
		}
	}

	/**
	 * Gibt die Praeferenz zurueck
	 * 
	 * @return preference
	 */
	public double getPreference() {
		return preference;
	}

	public double getTrainUse() {
		return overseer.getUse(ProjectBuilder.TRAIN);
	}

	public double getCarUse() {
		return overseer.getUse(ProjectBuilder.CAR);
	}
}
